import requests
import random
from bs4 import BeautifulSoup

class Lyric():
    user_agents = [
        'Mozilla/5.0 (Windows; U; Windows NT 5.1; it; rv:1.8.1.11) Gecko/20071127 Firefox/2.0.0.11',
        'Mozilla/5.0 (iPad; CPU OS 8_4_1 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12H321 Safari/600.1.4',
        'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)',
        'Mozilla/5.0 (compatible; Konqueror/3.5; Linux) KHTML/3.5.5 (like Gecko) (Kubuntu)',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/14.14393'
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:11.0) Gecko/20100101 Firefox/11.0',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:53.0) Gecko/20100101 Firefox/53.0',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'
    ]

    headers = {

    }

    def __init__(self, request):
        self.request = request

    def getSong(self, url):
        r = self.request.get(url, headers={'User-Agent': random.choice(self.user_agents)})
        soup = BeautifulSoup(r.content.decode(), 'html.parser')
        body = soup.select('.col-xs-12.col-lg-8.text-center')[0]
        song_title = body.find_all('b')
        artist = song_title[0].text.replace(" Lyrics", "")
        title = song_title[1].text
        lyrics = body.find_all('div', class_=None)[0].text

        return {
            'artist': artist,
            'title': title,
            'lyrics': lyrics
        }
        # print("{} - {}{}".format(artist, title, lyrics))

    def searchSong(self, query):
        r = self.request.get("https://search.azlyrics.com/search.php?q={}".format(query), headers={'User-Agent': random.choice(self.user_agents)})
        soup = BeautifulSoup(r.content.decode(), 'html.parser')

        selected_table = None
        tables = soup.select('.table.table-condensed')
        if len(tables) == 0:
            return []
        else:
            for table in tables:
                if "Song results" in table.text:
                    selected_table = table

            song_results = table.select('tr td.visitedlyr')
            results = []
            for song in song_results[:10]:
                results.append({
                    'title': song.select('a')[0].text,
                    'artist': song.select('b')[1].text,
                    'url': song.select('a')[0].get('href')
                })

            return results

    def getLyric(self, query):
        song_results = self.searchSong(query)
        if len(song_results) > 0:
            s = self.getSong(song_results[0]['url'])
            print(s)
            return "{} - {}{}".format(s['artist'], s['title'], s['lyrics'])
        else:
            return None
