import requests
import re
import json

def get_profile(username):
    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36',
    }

    r = requests.get('https://www.instagram.com/{}'.format(username), headers=headers)

    if r.status_code != 400:
        try:
            # pattern = re.compile()
            data = re.search(r'<script type="text\/javascript">window\._sharedData = (.*)<\/script>', r.text).group(1)
            data = json.loads(data.replace(";", ""))
            gdata = data['entry_data']['ProfilePage'][0]['graphql']
        
            if gdata['user']:
                return gdata['user']
            else:
                return []
        except Exception as e:
            print("Error when parse instagram profile")
            print(e)
            return []
    else:
        return []