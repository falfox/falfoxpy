import requests
import urllib
import random
import json
import math
import time
from PIL import Image, ImageDraw 

class Doodle:
    def __init__(self, base_url):
        self.base_url = base_url
        self.names = json.loads(open('doodlenames.txt', 'r').read())

    def get_doodle(self):
        picked = random.choice(self.names)
        url = "https://quickdraw.withgoogle.com/api"
        headers = {
            "Content-Type": "application/x-www-form-urlencoded",
            "origin": "https://quickdraw.withgoogle.com",
            "referer": "https://quickdraw.withgoogle.com/",
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36",
        }
        r = requests.post(url, data=urllib.parse.urlencode({
            'method': "gallery",
            'word': picked,
            'locale': "en_US",
            'edition': "",
        }), headers=headers)

        images = r.json()['images']
        image = json.loads(random.choice(images)['image'])

        strokes = [i for i in image]
        xs = [c[0] for c in strokes]
        ys = [c[1] for c in strokes]

        origX = math.floor(min(min(ys))) - 50
        origY = math.floor(min(min(ys))) - 50

        width = math.floor(max(max(xs))) + 50
        height = math.floor(max(max(ys))) + 50

        image1 = Image.new("RGB", (width, height), (255, 255, 255))
        draw = ImageDraw.Draw(image1)

        for s in strokes:
            xs = s[0]
            ys = s[1]

            lines = []
            # print(ys)
            for i in range(len(xs)):
                lines.append(xs[i] - origX)
                lines.append(ys[i] - origY)

            # print(lines)
            if len(lines) > 3:
                draw.line(lines, width=3, fill=(0, 0, 0))
                # w.create_line(*lines, width=3)

        filename = "{}.png".format(time.time())
        image1.save("doodles/{}".format(filename))
        return filename, picked
