import requests
from xml.etree import ElementTree

def search_song(query):
    r = requests.get("https://www.googleapis.com/youtube/v3/search?key=AIzaSyDFDF425K60CVT5XThzRSJ1rN3ibGIu29o&part=snippet&maxResults=3&type=music&q={}".format(query))

    if r.status_code == 200:
        items = r.json()['items']
        if len(items) > 1:
            return items
        else:
            return []
    else:
        print("Failed to search song")

def get_lyric(item):
    video_id = item['id']['videoId']
    title = item['snippet']['title']
    res = 'A'.join(str(ord(c)+13) for c in video_id)
    res = res+"A"
    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36',
        'Host': 'extension.musixmatch.com',
    }

    r = requests.get("https://extension.musixmatch.com?res=${}&hl=en-US&v=${}type=track&lang=en&name&kind&fmt=1".format(res, video_id), headers=headers)
    content = r.content
    try:
        xml = ElementTree.fromstring(content)
        lyrics = "Lyric {}\n\n".format(title) + "\n".join([line.text for line in xml.findall('text')]).replace("&apos;", "\'")
        return lyrics
    except Exception as e: 
        print('Failed parse XML')
        print(e)
        return []

def get_single_lyric(query):
    items = search_song(query)
    if not not items:
        item = items[0]
        lyrics = get_lyric(item)
        if not lyrics:
            video_id = item['id']['videoId']
            title = item['snippet']['title']
            lyrics = get_caption(video_id, title)
        
        return lyrics
    else:
        return items

def get_lyric_by_id(videoId, title):
    item = {
        'id': {
            'videoId': videoId
        },
        'snippet': {
            'title': title
        }
    }
    lyrics = get_lyric(item)
    if not lyrics:
        lyrics = get_caption(videoId, title)
    
    return lyrics


def get_caption(videoid, title):
    r = requests.get('https://www.youtube.com/api/timedtext?key=yttt1&sparams=asr_langs,caps,v&caps=asr&asr_langs=en,ja&v={}&hl=en_US&lang=en&fmt=srv3'.format(videoid))

    if r.status_code != 404:
        content = r.content
        if len(content) > 0:
            try:
                xml = ElementTree.fromstring(content)
                lyrics = "Lyric {}\n\n".format(title) + "\n".join([line.text for line in xml.findall('body/p')])
                return lyrics
            except Exception as e: 
                print("Failed parse Youtube XML")
                print(e)
                return []