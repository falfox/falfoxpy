
import errno
import json
import logging
import os
import sys
import tempfile
import redis
import time
import requests
from argparse import ArgumentParser

from flask import Flask, abort, request, send_from_directory
from linebot import LineBotApi, WebhookHandler
from linebot.exceptions import InvalidSignatureError, LineBotApiError
from linebot.models import (
    AudioMessage, BeaconEvent, BoxComponent,
    BubbleContainer, ButtonComponent, ButtonsTemplate,
    CameraAction, CameraRollAction, CarouselColumn,
    CarouselContainer, CarouselTemplate, ConfirmTemplate,
    DatetimePickerAction, FileMessage, FlexSendMessage,
    FollowEvent, IconComponent, ImageCarouselColumn,
    ImageCarouselTemplate, ImageComponent,
    ImageMessage, ImageSendMessage, JoinEvent,
    LeaveEvent, LocationAction, LocationMessage,
    LocationSendMessage, MessageAction, MessageEvent,
    PostbackAction, PostbackEvent, QuickReply,
    QuickReplyButton, SeparatorComponent, SourceGroup,
    SourceRoom, SourceUser, SpacerComponent,
    StickerMessage, StickerSendMessage,
    TemplateSendMessage, TextComponent, TextMessage,
    TextSendMessage, UnfollowEvent, URIAction,
    VideoMessage, BubbleStyle, BlockStyle
)

from doodle import Doodle
from insta import get_profile
from music import (
    get_caption, get_lyric, get_lyric_by_id, get_single_lyric, search_song
)
from coupon import Coupon
from azlyric import Lyric

app = Flask(__name__)
# app.logger.addHandler(logging.StreamHandler(sys.stdout))
# app.logger.setLevel(logging.ERROR)

channel_secret = os.getenv('LINE_CHANNEL_SECRET', None)
channel_access_token = os.getenv('LINE_CHANNEL_ACCESS_TOKEN', None)

if channel_secret is None:
    print('Specify LINE_CHANNEL_SECRET as environment variable.')
    sys.exit(1)
if channel_access_token is None:
    print('Specify LINE_CHANNEL_ACCESS_TOKEN as environment variable.')
    sys.exit(1)

falfox = LineBotApi(channel_access_token)
handler = WebhookHandler(channel_secret)

r = redis.Redis(host='127.0.0.1', port=6379, db=0)

doodle = Doodle("https://line.bookreads.info/doodles/")
coupon = Coupon(requests.Session())
lyric = Lyric(requests)

@app.route('/doodles/<path:filename>')
def download_file(filename):
    return send_from_directory('doodles', filename, as_attachment=False)


@app.route('/username/<username>')
def check_username(username):
    print(username)
    try:
        if username != None:
            profile = falfox.get_profile(username)
        return str(profile)
    except LineBotApiError as e:
        print(e.error.message)
        return "ERROR 404"
    # return str({
    #     'nama': profile.display_name,
    #     'id': profile.user_id,
    #     'picture': profile.picture_url,
    #     'status': profile.status_message,
    # })


@app.route("/callback", methods=['POST'])
def callback():
    signature = request.headers['X-Line-Signature']

    body = request.get_data(as_text=True)
    try:
        handler.handle(body, signature)
    except LineBotApiError as e:
        print("Got exception from LINE Messaging API: %s" % e.message)
        for m in e.error.details:
            print("  %s: %s" % (m.property, m.message))
        print("\n")
    except InvalidSignatureError:
        abort(400)

    return 'OK'

@handler.add(MessageEvent, message=ImageMessage)
def handle_content_message(event):
    message_content = falfox.get_message_content(event.message.id)

    image_name = 'images/{}.jpg'.format(time.time())

    with open(image_name, 'wb') as fd:
        for chunk in message_content.iter_content():
            fd.write(chunk)

    print(image_name)


@handler.add(MessageEvent, message=TextMessage)
def handle_message(event):
    if event.type == "message":
        handleText(event)


def handleText(event):
    text = event.message.text.lower()
    groupId = None
    if event.source.type == "group":
        groupId = "G:{}".format(event.source.group_id)
    elif event.source.type == "room":
        groupId = "R:{}".format(event.source.room_id)
    else:
        groupId = "P:{}".format(event.source.user_id)

#    display_name = r.get(event.source.user_id)
#    print("1. {}|{}".format(str(display_name), event.source.user_id))
#    if display_name is not None:
#        print("2. " + str(display_name))
#    else:
#        if event.source.user_id is not None:
#            profile = falfox.get_profile(event.source.user_id)
#            print("3. " + str(type(display_name)))
#            if profile is not None:
#                display_name = profile.display_name
#                r.set(event.source.user_id, profile.display_name)
#            else:
#                display_name = "Anonymous"
#        else:
#            display_name = "Anonymous"

    print("[{}][{}] {}".format(groupId, event.source.user_id, event.message.text))
    cmd = text.split(" ")[0]
    if event.source.user_id == 'U29b9243b94f06951a52179b0aed5e34a':
        last = r.get('last_message')
        now = round(time.time())
        r.set('last_message', time.time())

        if last == None or now - int(float(last)) > 86400:
            falfox.reply_message(
                event.reply_token,
                TextSendMessage(
                    text="Halo Bos"
                )
            )

    sourceId = None
    if isinstance(event.source, SourceGroup):
        sourceId = event.source.group_id
    elif isinstance(event.source, SourceRoom):
        sourceId = event.source.room_id
    else:
        sourceId = event.source.user_id

    answer = r.get(sourceId)

    if answer != None and answer.decode() == text.strip():
        print(answer)

        profile = None

        try:
            profile = falfox.get_profile(str(event.source.user_id))
        except LineBotApiError as e:
            print(e.error.message)

        r.delete(sourceId)

        display_name = profile.display_name if profile != None else "Seseorang yang belum add FalFox"
        picture_url = profile.picture_url if profile != None else "https://a.disquscdn.com/1544735374/images/noavatar92.png"
        bubble = BubbleContainer(
            body=BoxComponent(
                layout='vertical',
                margin=None,
                contents=[
                    TextComponent(
                        text="BENAR",
                        weight="bold",
                        color="#FD750A",
                        size="sm",
                        align="center"
                    ),
                    BoxComponent(
                        layout='vertical',
                        contents=[
                            SpacerComponent(
                                size="xl"
                            ),
                            ImageComponent(
                                url=picture_url,
                                size="full",
                                aspect_ratio="2:1"
                            ),
                            SpacerComponent(
                                size="xl"
                            ),
                        ]
                    ),
                    TextComponent(
                        text=display_name,
                        weight="bold",
                        size="md",
                        align="center",
                        wrap=True
                    ),
                    TextComponent(
                        text="Menjawab dengan benar! 👏",
                        size="sm",
                        align="center",
                        wrap=True
                    )
                ]
            ),
            footer=BoxComponent(
                layout="vertical",
                contents=[
                    SpacerComponent(
                        size="sm"
                    ),
                    ButtonComponent(
                        height="sm",
                        action=PostbackAction(
                            label="Mulai lagi",
                            data=json.dumps(
                                {"type": "doodle"}
                            ),
                            text="doodle start"
                        ),
                        style="primary",
                        color="#FD750A"
                    )
                ]
            )
        )

        message = FlexSendMessage(
            contents=bubble,
            alt_text="{} menjawab dengan benar!".format(display_name)
        )

        falfox.reply_message(
            event.reply_token,
            message
        )
        return

    if text == 'falfox' or text == 'help':
        sendMenu(event)
    elif text == 'that\'s pretty cringe bro':
        falfox.reply_message(
            event.reply_token,
            ImageSendMessage(
                original_content_url='https://image.prntscr.com/image/-weSVrw1TD6wEw-ah4F3nw.jpg',
                preview_image_url='https://image.prntscr.com/image/-weSVrw1TD6wEw-ah4F3nw.jpg'
            )
        )
    else:
        if text == 'test' or text == 'tes':
            falfox.reply_message(
                event.reply_token,
                TextSendMessage(
                    text='in',
                    quick_reply=QuickReply(
                        items=[
                            QuickReplyButton(action=MessageAction(
                                label="in", text="in"), image_url='https://raw.githubusercontent.com/google/material-design-icons/master/action/2x_web/ic_check_circle_white_18dp.png'),
                            QuickReplyButton(action=MessageAction(
                                label="cot", text="I'm Gay"), image_url='https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/twitter/154/angry-face_1f620.png'),
                            QuickReplyButton(action=MessageAction(
                                label="That's pretty cringe", text="I'm Gay"), image_url='https://www.stickpng.com/assets/thumbs/5845e755fb0b0755fa99d7f3.png')
                        ]
                    )
                )
            )

#        elif cmd == 'sourcecode':
#            falfox.reply_message(
#                event.reply_token,
#                TextSendMessage(
#                    text="https://gitlab.com/falfox/falfoxpy"
#                )
#            )

        elif cmd == 'insta':
            if text == 'insta':
                falfox.reply_message(
                    event.reply_token,
                    TextSendMessage(
                        text="Ketik \"insta <username>\" untuk mencari akun instagram"
                    )
                )
            else:
                query = text[len(cmd) + 1:]
                user = get_profile(query)
                if not user:
                    falfox.reply_message(
                        event.reply_token,
                        TextSendMessage(
                            text="Username tidak ditemukan"
                        )
                    )
                else:
                    bubble = BubbleContainer(
                        direction='ltr',
                        body=BoxComponent(
                            layout='horizontal',
                            margin=None,
                            contents=[
                                ImageComponent(
                                    url=user['profile_pic_url'],
                                    align='start',
                                    size='xl',
                                    aspect_mode='cover'
                                ),
                                BoxComponent(
                                    layout='vertical',
                                    spacing='md',
                                    contents=[
                                        BoxComponent(
                                            layout='vertical',
                                            contents=[
                                                TextComponent(
                                                    text=str(
                                                        user['edge_owner_to_timeline_media']['count']
                                                    ),
                                                    weight='bold',
                                                    align='end'
                                                ),
                                                TextComponent(
                                                    text='posts',
                                                    size='sm',
                                                    align='end'
                                                )
                                            ]
                                        ),
                                        BoxComponent(
                                            layout='vertical',
                                            contents=[
                                                TextComponent(
                                                    text=suffixNumber(
                                                        user['edge_followed_by']['count']
                                                    ),
                                                    weight='bold',
                                                    align='end'
                                                ),
                                                TextComponent(
                                                    text='followers',
                                                    size='sm',
                                                    align='end'
                                                )
                                            ]
                                        ),
                                        BoxComponent(
                                            layout='vertical',
                                            contents=[
                                                TextComponent(
                                                    text=suffixNumber(
                                                        user['edge_follow']['count']
                                                    ),
                                                    weight='bold',
                                                    align='end'
                                                ),
                                                TextComponent(
                                                    text='following',
                                                    size='sm',
                                                    align='end'
                                                )
                                            ]
                                        )
                                    ]
                                )
                            ]
                        ),
                        footer=BoxComponent(
                            layout='vertical',
                            contents=[
                                SeparatorComponent(),
                                TextComponent(
                                    text=('(🔒) ' if user['is_private'] else " ") + user['full_name'] + (
                                        ' ✓' if user['is_verified'] else " "),
                                    weight='bold',
                                    size='sm'
                                    # (user.is_private ? "(Private) " : " ") + user.full_name + (user.is_verified ? " ✓" : " ")
                                ),
                                TextComponent(
                                    text=user['biography'] if user['biography'] else " ",
                                    wrap=True,
                                    size='sm'
                                ),
                                TextComponent(
                                    text=user['external_url'] if user['external_url'] else " ",
                                    size='sm'
                                ),
                                ButtonComponent(
                                    style='primary',
                                    color='#FD750A',
                                    height='sm',
                                    margin='md',
                                    action=URIAction(
                                        label="Follow @{}".format(
                                            user['username']
                                        ),
                                        uri="https://www.instagram.com/{}".format(
                                            user['username']
                                        )
                                    )
                                )
                            ]
                        )
                    )

                    message = FlexSendMessage(
                        alt_text="@{} instagram profile".format(
                            user['username']
                        ), contents=bubble
                    )

                    falfox.reply_message(
                        event.reply_token,
                        message
                    )

        elif cmd == "lyric":
            if text == 'lyric':
                falfox.reply_message(
                    event.reply_token,
                    TextSendMessage(
                        text="Ketik \"lyric <judul lagu>\" untuk mencari lirik"
                    )
                )
            else:
                query = text[len(cmd) + 1:]
                print("Finding lyric {}".format(query))
                lyrics = lyric.getLyric(query)
                if not lyrics:
                    falfox.reply_message(
                        event.reply_token,
                        TextSendMessage(
                            text="Lirik tidak ditemukan"
                        )
                    )
                else:
                    lyrics = splitLyric(lyrics)
                    falfox.reply_message(
                        event.reply_token,
                         [TextSendMessage(text=l[:2000]) for l in lyrics]
                    )
        elif cmd == "lyrics":
            if text == 'lyrics':
                falfox.reply_message(
                    event.reply_token,
                    TextSendMessage(
                        text="Ketik \"lyrics <judul lagu>\" untuk mencari lirik"
                    )
                )
            else:
                query = text[len(cmd) + 1:]
                items = lyric.searchSong(query)
                columns = []
                if not items:
                    falfox.reply_message(
                        event.reply_token,
                        TextSendMessage(text="Lyrics tidak ditemukan")
                    )
                else:
                    for item in items:
                        column = BubbleContainer(
                            styles=BubbleStyle(
                                footer=BlockStyle(
                                    separator=True
                                )
                            ),
                            body=BoxComponent(
                                layout='vertical',
                                contents=[
                                    TextComponent(
                                        color='#FD750A',
                                        text="SONG",
                                        weight="bold",
                                        size="sm"
                                    ),
                                    TextComponent(
                                        text=item['title'],
                                        weight='bold',
                                        size='xl',
                                        margin="md",
                                        wrap=True
                                    ),
                                    TextComponent(
                                        text=item['artist'],
                                        size='sm',
                                        wrap=True
                                    )
                                ]
                            ),
                            footer=BoxComponent(
                                layout='vertical',
                                spacing='sm',
                                flex= 0,
                                contents=[
                                    ButtonComponent(
                                        style='link',
                                        height='sm',
                                        color='#FD750A',
                                        action=PostbackAction(
                                            label="See Lyric",
                                            data=json.dumps({
                                                'type': 'see_lyric',
                                                'url': item['url']
                                            }),
                                            text="See Lyric {} - {}".format(item['artist'], item['title'])
                                        )
                                    )
                                ]
                            )
                        )

                        columns.append(column)

                    carousel = CarouselContainer(
                        contents=columns
                    )
                    message = FlexSendMessage(
                        alt_text='Lyrics search results', contents=carousel
                    )
                    falfox.reply_message(event.reply_token, message)

        elif cmd == 'bye':
            if isinstance(event.source, SourceGroup):
                falfox.reply_message(
                    event.reply_token, StickerSendMessage(
                        package_id='11538',
                        sticker_id='51626533'
                    )
                )
                falfox.leave_group(event.source.group_id)
            elif isinstance(event.source, SourceRoom):
                falfox.reply_message(
                    event.reply_token, StickerSendMessage(
                        package_id='11538',
                        sticker_id='51626533'
                    )
                )
                falfox.leave_room(event.source.room_id)
            else:
                falfox.reply_message(
                    event.reply_token,
                    TextSendMessage(
                        text="FalFox tidak bisa keluar dari personal chat"
                    )
                )

        elif cmd == 'doodle':
            query = text[len(cmd) + 1:]
            sourceId = None

            print(query)

            sourceId = get_source_id(event)

            if text == 'doodle':
                falfox.reply_message(
                    event.reply_token,
                    TextSendMessage(
                        text="Cara bermain: \ntebak gambar doodle yang dikirim, dalam bahasa inggris\n\nMenu:\ndoodle start - memulai\ndoodle skip - skip doodle\ndoodle stop - akhiri permainan")
                )
            elif query == 'start':
                start_doodle(event, sourceId)
            elif query == 'stop':
                if r.get(sourceId) == None:
                    falfox.reply_message(
                        event.reply_token,
                        TextMessage(text="Permainan belum dimulai")
                    )
                else:
                    answer = r.get(sourceId).decode()
                    r.delete(sourceId)
                    falfox.reply_message(
                        event.reply_token,
                        TextMessage(
                            text="Permainan berakhir, jawabannya adalah {}".format(answer))
                    )
            elif query == 'skip':
                if r.get(sourceId) == None:
                    falfox.reply_message(
                        event.reply_token,
                        TextMessage(text="Permainan belum dimulai")
                    )
                else:
                    answer = r.get(sourceId).decode()
                    filename, picked = doodle.get_doodle()
                    r.set(sourceId, picked)
                    print(picked)
                    print(sourceId)
                    falfox.reply_message(
                        event.reply_token,
                        messages=[
                            TextMessage(
                                text="Jawaban sebelumnya adalah {}".format(
                                    answer)
                            ),
                            ImageSendMessage(
                                preview_image_url=doodle.base_url + filename,
                                original_content_url=doodle.base_url + filename
                            )
                        ]
                    )

        elif cmd == 'author':
            bubble = BubbleContainer(
                body=BoxComponent(
                    layout='vertical',
                    margin=None,
                    contents=[
                        BoxComponent(
                            layout='vertical',
                            contents=[
                                TextComponent(
                                    text="Developer",
                                    weight='bold',

                                ),
                                TextComponent(
                                    text='Ridho Maulana',
                                    size='sm',
                                    color='#707070'
                                )
                            ]
                        ),
                        BoxComponent(
                            layout='vertical',
                            contents=[
                                TextComponent(
                                    text='Contacts',
                                    weight='bold',
                                ),
                                BoxComponent(
                                    layout='horizontal',
                                    contents=[
                                        ButtonComponent(
                                            action=URIAction(
                                                label='LINE',
                                                uri='https://line.me/ti/p/~falfox'
                                            ),
                                            style='primary',
                                            color='#02BE02',
                                            height='sm',
                                            margin='md',
                                            flex=1
                                        ),
                                        ButtonComponent(
                                            action=URIAction(
                                                label='FOLLOW',
                                                uri='https://www.instagram.com/ridhoomm'
                                            ),
                                            style='primary',
                                            color='#FD750A',
                                            height='sm',
                                            margin='md',
                                            flex=1
                                        )
                                    ]
                                )
                            ]
                        )
                    ]
                )
            )

            message = FlexSendMessage(
                alt_text="Author info",
                contents=bubble
            )

            falfox.reply_message(
                event.reply_token,
                message
            )

        elif cmd == 'promo':
            query = text[len(cmd) + 1:]

            if text == 'promo':
                falfox.reply_message(
                    event.reply_token,
                    TextSendMessage(
                        text="Dapatkan promo terbaru \n\nMenu:\n - promo list - melihat daftar kategori promo\n - promo <kategori> - melihat promo yang tersedia\n\nContoh: promo grab"
                    )
                )
            elif query == 'list':
                text_to_send = "Daftar Kategori Promo:\n"
                for category in coupon.getProducts():
                    text_to_send += "\n- {}".format(category)

                falfox.reply_message(
                    event.reply_token,
                    TextSendMessage(
                        text=text_to_send
                    )
                )
            else:
                coupons = coupon.getPromos(query)
                if len(coupons) == 0:
                    falfox.reply_message(
                        event.reply_token,
                        TextSendMessage(
                            text="Kategori promo tidak ditemukan atau sedang tidak ada promo"
                        )
                    )
                else:
                    columns = []
                    for coup in coupons:
                        column = BubbleContainer(
                            hero=ImageComponent(
                                url=coup['image'],
                                size='full',
                                aspect_ratio='2:1',
                                aspect_mode='fit'
                            ),
                            body=BoxComponent(
                                layout='vertical',
                                contents=[
                                    SeparatorComponent(),
                                    TextComponent(
                                        text=coup['title'],
                                        weight='bold',
                                        size='xl'
                                    ),
                                    TextComponent(
                                        text=coup['desc'],
                                        size='md',
                                        wrap=True
                                    ),
                                    SeparatorComponent()
                                ]
                            ),
                            footer=BoxComponent(
                                layout='vertical',
                                spacing='sm',
                                flex= 0,
                                contents=[
                                    ButtonComponent(
                                        style='link',
                                        height='sm',
                                        color='#FD750A',
                                        action=PostbackAction(
                                            label="Lihat Detail",
                                            data=json.dumps({
                                                'type': 'promo_detail',
                                                'id': coup['id']
                                            })
                                        )
                                    ),
                                    ButtonComponent(
                                        style='link',
                                        height='sm',
                                        color='#FD750A',
                                        action=PostbackAction(
                                            label="Lihat Kode",
                                            data=json.dumps({
                                                'type': 'promo_code',
                                                'id': coup['id']
                                            })
                                        )
                                    )
                                ]
                            )
                        )
                        columns.append(column)

                    carousel = CarouselContainer(
                        contents=columns
                    )
                    message = FlexSendMessage(
                        contents=carousel,
                        alt_text="Promo dari {}".format(query)
                    )
                    
                    falfox.reply_message(
                        event.reply_token,
                        message
                    )

@handler.add(FollowEvent)
def handle_follow(event):
    falfox.reply_message(
        event.reply_token,
        TextSendMessage(
            text="Hai, namaku FalFox, ketik help untuk melihat menu"
        )
    )

@handler.add(JoinEvent)
def handle_join(event):
    falfox.reply_message(
        event.reply_token,
        TextSendMessage(
            text="Hai, namaku FalFox, ketik help untuk melihat menu"
        )
    )


@handler.add(PostbackEvent)
def handle_postback(event):
    data = json.loads(event.postback.data)
    pbType = data['type']

    # sourceId = get_source_id(event)

    if pbType == 'lyric':
        videoId = data['id']
        title = data['title']
        lyrics = get_lyric_by_id(videoId, title)
        if not lyrics:
            falfox.reply_message(
                event.reply_token,
                TextSendMessage(
                    text="Lirik tidak ditemukan"
                )
            )
        else:
            falfox.reply_message(
                event.reply_token,
                TextSendMessage(
                    text=lyrics[:2000]
                )
            )
    elif pbType == 'see_lyric':
        url = data['url']
        song = lyric.getSong(url)
        lyrics = splitLyric("{} - {}{}".format(song['artist'], song['title'], song['lyrics']))
        
        falfox.reply_message(
            event.reply_token,
            [TextSendMessage(text=l[:2000]) for l in lyrics]
        )

    elif pbType == 'promo_detail':
        couponId =  data['id']
        promo_detail = coupon.getCouponDetail(couponId)
        text_to_send = "Detail Promo\n\n"
        text_to_send += promo_detail
        falfox.reply_message(
            event.reply_token,
            TextSendMessage(
                text=text_to_send
            )
        )
    elif pbType == 'promo_code':
        couponId =  data['id']
        promo_code, promo_desc = coupon.getCouponCode(couponId)
        falfox.reply_message(
            event.reply_token,
            [
                TextSendMessage(
                    text=promo_desc
                ),
                TextSendMessage(
                    text=promo_code
                )
            ]
        )

    # elif pbType == 'doodle':
        # start_doodle(event, sourceId)


def start_doodle(event, sourceId):
    if r.get(sourceId) != None:
        falfox.reply_message(
            event.reply_token,
            TextMessage(text="Permainan sudah dimulai")
        )
    else:
        filename, picked = doodle.get_doodle()
        r.set(sourceId, picked)
        print(picked)
        falfox.reply_message(
            event.reply_token,
            messages=ImageSendMessage(
                preview_image_url=doodle.base_url + filename,
                original_content_url=doodle.base_url + filename,
            )
        )


def get_source_id(event):
    if isinstance(event.source, SourceGroup):
        sourceId = event.source.group_id
    elif isinstance(event.source, SourceRoom):
        sourceId = event.source.room_id
    else:
        sourceId = event.source.user_id

    return sourceId


def suffixNumber(num):
    if abs(num) < 10 ** 3:
        return "{}".format(num)
    elif abs(num) < 10 ** 6:
        return "{}K".format(round(num / (10 ** 3)))
    else:
        return "{}M".format(round(num / (10 ** 6)))


def splitLyric(lyric):
    lyrics = []
    curr = ""
    splitted = lyric.splitlines()
    for index, line in enumerate(splitted):
        if len(curr) + len(line) < 2000:
            curr += "{}\n".format(line)
        else:
            lyrics.append(curr)
            curr = ""
            curr += "{}\n".format(line)
        
        if index == len(splitted) - 1:
            lyrics.append(curr)
            

    return lyrics
        
# @handler.add(MessageEvent, message=(ImageMessage, VideoMessage, AudioMessage))
# def handle_content_message(event):
#     if isinstance(event.message, ImageMessage):
#         ext = 'jpg'
#     elif isinstance(event.message, VideoMessage):
#         ext = 'mp4'
#     elif isinstance(event.message, AudioMessage):
#         ext = 'm4a'
#     else:
#         return

#     message_content = falfox.get_message_content(event.message.id)
#     with tempfile.NamedTemporaryFile(dir=static_tmp_path, prefix=ext + '-', delete=False) as tf:
#         for chunk in message_content.iter_content():
#             tf.write(chunk)
#         tempfile_path = tf.name

#     dist_path = tempfile_path + '.' + ext
#     dist_name = os.path.basename(dist_path)
#     os.rename(tempfile_path, dist_path)

#     falfox.reply_message(
#         event.reply_token, [
#             TextSendMessage(text='Save content.'),
#             TextSendMessage(text=request.host_url + os.path.join('static', 'tmp', dist_name))
#         ]
#     )


def sendMenu(event):
    falfox.reply_message(
        event.reply_token,
        TextSendMessage(text="Command List:\n➡  promo - mendapatkan promo terkini\n➡  doodle - Game tebak doodle\n➡  insta - Mencari akun instagram\n➡  lyric - Cari lirik dari lagu\n➡  lyrics - Cari berbagai lagu\n➡  bye - Kick bot\n➡  author - Info mengenai author\n➡  help - Daftar command")
        # \n➡  gag - Random 9gag post
        # \n🆕 doodle - Game tebak doodle
    )


if __name__ == "__main__":
    # make_static_tmp_dir()
    app.run(debug=True)
