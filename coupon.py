import requests
from pprint import pprint
from bs4 import BeautifulSoup

class Coupon:
    product_codes = {
        'grab': 'grab-kode-promo',
        'grabfood':'grabfood',
        'Grab':'grab-kode-promo',
        'tokopedia':'tokopedia-voucher',
        'shopee':'shopee-kode-promo',
        'lazada':'lazada-voucher',
        'zalora':'zalora-voucher',
        'blibli':'blibli-voucher',
        'tiket.com':'tiket-com-kode-promo',
        'fave':'fave-kode-promo',
        'ruparupa':'ruparupa-voucher',
        'hp store':'hp-store-kode-promo',
        'bukalapak':'bukalapak-voucher',
        'traveloka':'traveloka-kupon',
        'gojek':'gojek-voucher',
        'berrybenka':'berrybenka',
        'agoda':'agoda-promo',
        'urban icon':'urban-icon-promo',
        'the body shop':'the-body-shop-voucher',
        'reebok':'reebok-promo',
        'indosat':'indosat-promo'
    }

    baseUrl = 'https://www.cuponation.co.id'
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36'
    }

    def __init__(self, request):        
        cookies = {
            '_gcl_au': '1.1.2100284661.1550307935',
            'ads': 'true',
            '_ga': 'GA1.3.339509916.1550307935',
            '_gid': 'GA1.3.1610561392.1550307935',
            'customerHash': '3c18e86e-9301-45c0-b174-c7e1ffbd45b2',
            'first_time_seen': 'true',
            'fe_visitor_session': '20ec5059-da71-4683-abee-29ab19aaccc21550311252153',
            'session-xt32g93946b19j93890506028438f089': '56971161-b451-464f-bc0e-dc24bd9cdc77',
            '_gat': '1'
        }

        for key in cookies.keys():
            request.cookies[key] = cookies[key]

        self.s = request

    def getPromos(self, productCode):
        r = self.s.get('{}/{}'.format(self.baseUrl,
                                 self.product_codes[productCode]), headers=self.headers)
        
        if r.status_code != 200:
            return []

        soup = BeautifulSoup(r.content.decode(), 'html.parser')
        coupons = soup.select('.main_vouchers div.v.c')
        image = soup.select('.retailer-image img')[0].get('src')

        valid_coupons = []

        for c in coupons[:10]:
            c_type = c.select('.plate img')
            if len(c_type) > 0:
                if 'code.svg' in c_type[0].get('data-src'):
                    couponId = c.get('id').replace('v-', '')
                    title = c.find('div', class_='tit')
                    if not title is None:
                        title = ' '.join(list(title.stripped_strings))
                        desc = c.find('h3', class_='caption').text.strip()
                        valid_coupons.append({
                            'id': couponId,
                            'title': title,
                            'desc': desc,
                            'image': self.getImageUrl(image),
                            # 'code': self.getCouponById(couponId)
                        })
        return valid_coupons

    def getCouponCode(self, couponId):
        # s.cookies.set('voucher_co', '32285', domain='.cuponation.co.id', path='/')
        self.s.cookies['voucher_co'] = str(couponId)
        r = self.s.get('{}/v/{}'.format(self.baseUrl, couponId),
                  headers=self.headers)
        soup = BeautifulSoup(r.content.decode(), 'html.parser')
        return (
            soup.find('div', class_='copy-code').get('data-clipboard-text'),
            soup.select('.details .container')[0].find(text=True, recursive=False).strip()
        ) 

    def getCouponDetail(self, couponId):
        self.s.cookies['voucher_co'] = str(couponId)
        r = self.s.get('{}/v/{}'.format(self.baseUrl, couponId),
                  headers=self.headers)
        soup = BeautifulSoup(r.content.decode(), 'html.parser')
        captions = soup.find('div', class_='additional-captions')
        for _ in captions.find_all('br'):
            captions.br.replace_with("\n")
        return captions.text

    def getImageUrl(self, url):
        return url.split('x0/')[1].replace("1.png", "9.png")

    def getProducts(self):
        return ["GrabFood", "Grab", "Tokopedia", "Shopee", "Lazada", "Zalora", "Blibli", "Tiket.com", "Fave", "Ruparupa", "HP Store", "Bukalapak", "Traveloka", "Gojek", "Berrybenka", "Agoda", "Urban Icon", "The Body Shop", "Reebok", "Indosat"]